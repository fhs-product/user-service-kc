package com.fhs.platform.service.user.constants.maritalstatus;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MaritalStatus {
    Divorced("D", "Divorced"),
    Unknown("UNK", "Unknown"),
    Married("M", "Married"),
    Single("S", "Single");
    private final String code;
    private final String description;
}
