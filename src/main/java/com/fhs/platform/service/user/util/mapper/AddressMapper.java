package com.fhs.platform.service.user.util.mapper;

import com.fhs.platform.service.user.controller.request.create.AddressCreateDto;
import com.fhs.platform.service.user.controller.request.update.AddressUpdateDto;
import com.fhs.platform.service.user.controller.response.AddressResponseDto;
import com.fhs.platform.service.user.entity.Address;
import org.mapstruct.BeanMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = {
}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface AddressMapper {

    @Mapping(target = "patient", ignore = true)
    Address addressDtoToAddress(AddressCreateDto addressCreateDTO);

    AddressResponseDto addressToAddressResponseDto(Address address);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateAddressFromDto(AddressUpdateDto addressUpdateDto, @MappingTarget Address address);

}
