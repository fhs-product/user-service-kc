package com.fhs.platform.service.user.constants.addressuse;

import java.util.stream.Stream;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class AddressUseConverter implements AttributeConverter<AddressUse, String> {

    @Override
    public String convertToDatabaseColumn(AddressUse addressTypeConverter) {
        if (addressTypeConverter == null) {
            return null;
        }
        return addressTypeConverter.getCode();
    }

    @Override
    public AddressUse convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
        return Stream.of(AddressUse.values()).filter(c -> c.getCode().equals(code)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

}

