package com.fhs.platform.service.user.constants.addressuse;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AddressUse {
    Home("home", "Home address"),
    Work("work", "Work address"),
    Temp("temp", "Temporary address"),
    Old("old", "Old address");
    private final String code;
    private final String description;
}
