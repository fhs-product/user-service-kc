package com.fhs.platform.service.user.constants.gender;


import java.util.stream.Stream;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class GenderConverter implements AttributeConverter<Gender, Character> {

    @Override
    public Character convertToDatabaseColumn(Gender gender) {
        if (gender == null) {
            return 'O';
        }
        return gender.getCode();
    }

    @Override
    public Gender convertToEntityAttribute(Character code) {
        if (code == null) {
            return null;
        }
        return Stream.of(Gender.values()).filter(c -> c.getCode().equals(code)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

}
