package com.fhs.platform.service.user.constants;

public class KeycloakIntegrationConstants {

    public static final String APP_USER_REALM_ROLE_NAME = "app-user";
    public static final String DEFAULT_PASSWORD = "pass";

}
