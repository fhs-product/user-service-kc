package com.fhs.platform.service.user.repository;

import com.fhs.platform.service.user.entity.Address;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, UUID> {

}
