package com.fhs.platform.service.user.constants.addresstype;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AddressType {
    Postal("postal", "Postal address"),
    Physical("physical", "Physical address"),
    Both("both", "Both");
    private final String code;
    private final String description;
}
