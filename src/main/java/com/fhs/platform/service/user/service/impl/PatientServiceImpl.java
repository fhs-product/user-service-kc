package com.fhs.platform.service.user.service.impl;


import com.fhs.platform.common.dto.DeleteResponse;
import com.fhs.platform.common.dto.PagingResponseDTO;
import com.fhs.platform.common.errors.exceptions.ResourceNotFoundException;
import com.fhs.platform.service.user.constants.UserConstants;
import com.fhs.platform.service.user.controller.PatientController;
import com.fhs.platform.service.user.controller.PractitionerController;
import com.fhs.platform.service.user.controller.request.create.AddressCreateDto;
import com.fhs.platform.service.user.controller.request.create.ContactCreateDto;
import com.fhs.platform.service.user.controller.request.create.IdentifierCreateDto;
import com.fhs.platform.service.user.controller.request.create.PatientCreateDto;
import com.fhs.platform.service.user.controller.request.update.AddressUpdateDto;
import com.fhs.platform.service.user.controller.request.update.ContactUpdateDto;
import com.fhs.platform.service.user.controller.request.update.IdentifierUpdateDto;
import com.fhs.platform.service.user.controller.request.update.PatientUpdateDto;
import com.fhs.platform.service.user.controller.response.AddressResponseDto;
import com.fhs.platform.service.user.controller.response.ContactResponseDto;
import com.fhs.platform.service.user.controller.response.AttachmentResponseDto;
import com.fhs.platform.service.user.controller.response.IdentifierResponseDto;
import com.fhs.platform.service.user.controller.response.PatientResponseDto;
import com.fhs.platform.service.user.domain.PatientFilter;
import com.fhs.platform.service.user.entity.Address;
import com.fhs.platform.service.user.entity.Contact;
import com.fhs.platform.service.user.entity.Identifier;
import com.fhs.platform.service.user.entity.Patient;
import com.fhs.platform.service.user.exceptions.EntityAlreadyExists;
import com.fhs.platform.service.user.exceptions.EntityNotFoundException;
import com.fhs.platform.service.user.integration.IamServiceIntegration;
import com.fhs.platform.service.user.integration.KeycloakServiceIntegration;
import com.fhs.platform.service.user.integration.dto.IamUserDto;
import com.fhs.platform.service.user.integration.dto.KeycloakUserDto;
import com.fhs.platform.service.user.repository.AddressRepository;
import com.fhs.platform.service.user.repository.ContactRepository;
import com.fhs.platform.service.user.repository.IdentifierRepository;
import com.fhs.platform.service.user.repository.PatientRepository;
import com.fhs.platform.service.user.service.PatientService;
import com.fhs.platform.service.user.service.StorageService;
import com.fhs.platform.service.user.util.mapper.AddressMapper;
import com.fhs.platform.service.user.util.mapper.ContactMapper;
import com.fhs.platform.service.user.util.mapper.IdentifierMapper;
import com.fhs.platform.service.user.util.mapper.PatientMapper;
import com.fhs.platform.service.user.util.search.PatientSpecification;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

@Service
@RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;
    private final IdentifierRepository identifierRepository;
    private final AddressRepository addressRepository;
    private final ContactRepository contactRepository;
    private final PatientMapper patientMapper;
    private final AddressMapper addressMapper;
    private final IdentifierMapper identifierMapper;
    private final ContactMapper contactMapper;
    private final StorageService storageService;
    private final IamServiceIntegration iamServiceIntegration;
    private final KeycloakServiceIntegration keycloakServiceIntegration;


    @Override
    public PatientResponseDto createPatient(PatientCreateDto patientCreateDto) {
        Patient patient = patientMapper.patientDtoToPatient(patientCreateDto);
        KeycloakUserDto keycloakUserDto = KeycloakUserDto.patientToKeycloakUserDto(patient);
        keycloakServiceIntegration.createUser(keycloakUserDto);
        patient.setId(keycloakUserDto.getUserId());

        patient.getIdentifiers().forEach(identifier -> {
            if (identifierRepository.existsByIdentifierTypeAndValueAndIsDeleted(identifier.getIdentifierType(), identifier.getValue(), false)) {

                throw new EntityAlreadyExists(UserConstants.DUPLICATE_IDENTIFIER_MESSAGE_EXCEPTION, identifier.getIdentifierType(),
                        identifier.getValue());
            }
            identifier.setPatient(patient);
        });
        patient.getAddresses().forEach(address -> address.setPatient(patient));
        patient.getContacts().forEach(contact -> {
            contact.getAddress().setContact(contact);
            contact.setPatient(patient);
        });
        return patientMapper.patientToPatientResponseDto(patientRepository.save(patient));
    }

    @Override
    public PatientResponseDto updatePatientById(UUID patientId, PatientUpdateDto patientUpdateDto) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new EntityNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        patientMapper.updatePatientFromDto(patientUpdateDto, patient);

        return patientMapper.patientToPatientResponseDto(patientRepository.save(patient));
    }

    @Override
    public DeleteResponse deletePatientById(UUID patientId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new EntityNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));

        patient.setIsDeleted(true);

        return new DeleteResponse(Boolean.TRUE);
    }

    @Override
    public AddressResponseDto createPatientAddress(UUID patientId, AddressCreateDto addressCreateDto) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new EntityNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        Address newAddress = addressMapper.addressDtoToAddress(addressCreateDto);
        newAddress.setPatient(patient);

        patient.getAddresses().add(newAddress);
        Address savedAddress = addressRepository.save(newAddress);
        patientRepository.save(patient);

        return addressMapper.addressToAddressResponseDto(savedAddress);
    }

    @Override
    public PagingResponseDTO<?> getPatients(Pageable pageable, PatientFilter filter) {
        Page<Patient> patientPage = patientRepository.findAll(new PatientSpecification(filter), pageable);
        return new PagingResponseDTO<>(patientPage.map(patientMapper::patientToPatientResponseDto));
    }

    @Override
    public IdentifierResponseDto createPatientIdentifier(UUID patientId, IdentifierCreateDto identifierCreateDto) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        Identifier newIdentifier = identifierMapper.identifierDtoToIdentifier(identifierCreateDto);

        if (identifierRepository.existsByIdentifierTypeAndValueAndIsDeleted(newIdentifier.getIdentifierType(), newIdentifier.getValue(), false)) {
            throw new EntityAlreadyExists(UserConstants.DUPLICATE_IDENTIFIER_MESSAGE_EXCEPTION, newIdentifier.getIdentifierType(),
                    newIdentifier.getValue());
        }
        newIdentifier.setPatient(patient);
        patient.getIdentifiers().add(newIdentifier);

        Identifier savedIdentifier = identifierRepository.save(newIdentifier);
        patientRepository.save(patient);
        return identifierMapper.identifierToIdentifierResponseDto(savedIdentifier);

    }

    @Override
    public ContactResponseDto createPatientContact(UUID patientId, ContactCreateDto contactCreateDto) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new EntityNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        Contact newPatientContact = contactMapper.contactDtoToContact(contactCreateDto);
        Address newContactAddress = addressMapper.addressDtoToAddress(contactCreateDto.getAddress());
        newContactAddress.setContact(newPatientContact);
        newPatientContact.setAddress(newContactAddress);
        newPatientContact.setPatient(patient);
        patient.getContacts().add(newPatientContact);

        Contact savedContact = contactRepository.save(newPatientContact);
        AddressResponseDto addressResponseDto = addressMapper.addressToAddressResponseDto(savedContact.getAddress());
        ContactResponseDto contactResponseDto = contactMapper.contactToContactResponseDto(savedContact);
        contactResponseDto.setAddress(addressResponseDto);
        patientRepository.save(patient);
        return contactResponseDto;
    }

    @Override
    public List<AddressResponseDto> getPatientAddresses(UUID patientId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        return patient.getAddresses().stream().filter(address -> !address.getIsDeleted()).map(addressMapper::addressToAddressResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IdentifierResponseDto> getPatientIdentifiers(UUID patientId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        return patient.getIdentifiers().stream().filter(identifier -> !identifier.getIsDeleted())
                .map(identifierMapper::identifierToIdentifierResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ContactResponseDto> getPatientContacts(UUID patientId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));

        return patient.getContacts().stream().map(contactMapper::contactToContactResponseDto).collect(Collectors.toList());
    }

    @Override
    public AddressResponseDto updatePatientAddress(UUID patientId, UUID addressId, AddressUpdateDto addressUpdateDto) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        Address patientAddress = patient.getAddresses().stream()
                .filter(address -> (!address.getIsDeleted()) && address.getId().equals(addressId)).findFirst()
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.ADDRESS_NOT_FOUND_MESSAGE_EXCEPTION));
        addressMapper.updateAddressFromDto(addressUpdateDto, patientAddress);
        Address address = addressRepository.save(patientAddress);
        patientRepository.save(patient);

        return addressMapper.addressToAddressResponseDto(address);

    }

    @Override
    public IdentifierResponseDto updatePatientIdentifier(UUID patientId, UUID identifierId, IdentifierUpdateDto identifierUpdateDto) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        Identifier patientIdentifier = patient.getIdentifiers().stream()
                .filter(identifier -> (!identifier.getIsDeleted()) && identifier.getId().equals(identifierId))
                .findFirst().orElseThrow(() -> new EntityNotFoundException(UserConstants.IDENTIFIER_NOT_FOUND_MESSAGE_EXCEPTION));

        identifierMapper.updateIdentifierFromDto(identifierUpdateDto, patientIdentifier);

        if (identifierRepository
                .existsByIdentifierTypeAndValueAndIsDeleted(patientIdentifier.getIdentifierType(), patientIdentifier.getValue(), false)) {
            throw new EntityAlreadyExists(UserConstants.DUPLICATE_IDENTIFIER_MESSAGE_EXCEPTION, patientIdentifier.getIdentifierType(),
                    patientIdentifier.getValue());
        }
        Identifier identifier = identifierRepository.save(patientIdentifier);
        patientRepository.save(patient);
        return identifierMapper.identifierToIdentifierResponseDto
                (identifier);
    }

    @Override
    public ContactResponseDto updatePatientContact(UUID patientId, UUID contactId, ContactUpdateDto contactUpdateDto) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        Contact patientContact = patient.getContacts().stream()
                .filter(contact -> ((!contact.getIsDeleted()) && contact.getId().equals(contactId)))
                .findFirst()
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.CONTACT_NOT_FOUND_MESSAGE_EXCEPTION));

        addressMapper.updateAddressFromDto(contactUpdateDto.getAddress(), patientContact.getAddress());
        contactMapper.updateContactFromDto(contactUpdateDto, patientContact);

        Contact savedContact = contactRepository.save(patientContact);
        AddressResponseDto addressResponseDto = addressMapper.addressToAddressResponseDto(savedContact.getAddress());
        ContactResponseDto contactResponseDto = contactMapper.contactToContactResponseDto(savedContact);
        contactResponseDto.setAddress(addressResponseDto);

        patientRepository.save(patient);
        return contactResponseDto;
    }

    @Override
    public DeleteResponse deletePatientAddressById(UUID patientId, UUID addressId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        Address patientAddress = patient.getAddresses().stream()
                .filter(address -> (!address.getIsDeleted() && address.getId().equals(addressId))).findFirst()
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.ADDRESS_NOT_FOUND_MESSAGE_EXCEPTION));
        patientAddress.setIsDeleted(true);

        Address address = addressRepository.save(patientAddress);
        patientRepository.save(patient);
        return new DeleteResponse(Boolean.TRUE);
    }

    @Override
    public DeleteResponse deletePatientIdentifier(UUID patientId, UUID identifierId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        Identifier patientIdentifier = patient.getIdentifiers().stream()
                .filter(identifier -> (!identifier.getIsDeleted()) && identifier.getId().equals(identifierId))
                .findFirst().orElseThrow(() -> new ResourceNotFoundException(UserConstants.IDENTIFIER_NOT_FOUND_MESSAGE_EXCEPTION));
        patientIdentifier.setIsDeleted(true);
        Identifier identifier = identifierRepository.save(patientIdentifier);
        patientRepository.save(patient);
        return new DeleteResponse(Boolean.TRUE);
    }

    @Override
    public IdentifierResponseDto getPatientIdentifierById(UUID patientId, UUID identifierId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));

        return patient.getIdentifiers().stream()
                .filter(identifier -> (!identifier.getIsDeleted()) && identifier.getId().equals(identifierId))
                .map(identifierMapper::identifierToIdentifierResponseDto)
                .findFirst().orElseThrow(() -> new ResourceNotFoundException(UserConstants.IDENTIFIER_NOT_FOUND_MESSAGE_EXCEPTION));
    }

    @Override
    public DeleteResponse deletePatientContact(UUID patientId, UUID contactId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        Contact patientContact = patient.getContacts().stream()
                .filter(contact -> (!contact.getIsDeleted()) && contact.getId().equals(contactId))
                .findFirst().orElseThrow(() -> new ResourceNotFoundException(UserConstants.CONTACT_NOT_FOUND_MESSAGE_EXCEPTION));
        patientContact.getAddress().setIsDeleted(true);
        patientContact.setIsDeleted(true);

        patientRepository.save(patient);
        return new DeleteResponse(Boolean.TRUE);
    }

    @Override
    public AddressResponseDto getPatientAddressById(UUID patientId, UUID addressId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));

        return (patient.getAddresses().stream().filter(address -> (!address.getIsDeleted()) && address.getId().equals(addressId))
                .map(addressMapper::addressToAddressResponseDto).findFirst()
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.ADDRESS_NOT_FOUND_MESSAGE_EXCEPTION)));
    }

    @Override
    public ContactResponseDto getPatientContactById(UUID patientId, UUID contactId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new EntityNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));

        return patient.getContacts().stream().filter(contact -> (!contact.getIsDeleted()) && contact.getId().equals(contactId))
                .map(contactMapper::contactToContactResponseDto)
                .findFirst().orElseThrow(() -> new ResourceNotFoundException(UserConstants.CONTACT_NOT_FOUND_MESSAGE_EXCEPTION));
    }

    @Override
    public AttachmentResponseDto uploadAvatarToPatient(MultipartFile avatar, UUID patientId) {
        Patient patient = patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new EntityNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION));
        storageService.store(avatar, patientId);
        patient.setAvatarUrl(MvcUriComponentsBuilder
                .fromMethodName(PatientController.class, "getAvatar", avatar.getOriginalFilename(), patientId.toString()).build().toString());
        patientRepository.save(patient);
        return new AttachmentResponseDto(patient.getAvatarUrl());
    }

    @Override
    public Resource loadAvatarFile(String filename, UUID patientId) {
        return storageService.loadAsResource(filename, patientId);
    }

    @Override
    public PatientResponseDto getPatientById(UUID patientId) {
        return patientMapper.patientToPatientResponseDto(patientRepository.findByIdAndIsDeleted(patientId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PATIENT_NOT_FOUND_MESSAGE_EXCEPTION)));
    }

}
