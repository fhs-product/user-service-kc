package com.fhs.platform.service.user.controller.request.create;

import java.time.ZonedDateTime;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fhs.platform.common.config.jackson.ZonedDateTimeDeserializer;
import com.fhs.platform.common.config.jackson.ZonedDateTimeSerializer;
import com.fhs.platform.common.constants.DateTimeFormatConstants;
import lombok.Data;


@Data
public class PatientCreateDto {

    @NotBlank
    private String phoneNumber;
    @NotNull
    private String familyName;
    @NotNull
    private String givenName;
    @NotNull
    private String prefixName;
    private String suffixName;
    @NotNull
    private String email;
    @NotNull
    private String gender;
    @NotNull
    @JsonDeserialize(using = ZonedDateTimeDeserializer.class)
    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormatConstants.ZONE_DATE_TIME_FORMAT)
    private ZonedDateTime  birthDate;
    @NotNull
    private String maritalStatus;
    @NotNull
    private String managingOrganization;
    @Valid
    private Set<IdentifierCreateDto> identifiers;
    @Valid
    private Set<AddressCreateDto> addresses;
    @Valid
    private Set<ContactCreateDto> contacts;

}
