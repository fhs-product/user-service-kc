package com.fhs.platform.service.user.exceptions;


import com.fhs.platform.service.user.constants.identifiertype.IdentifierType;

public class EntityAlreadyExists extends RuntimeException {

    public EntityAlreadyExists(String message, IdentifierType type, String value) {
        super(String.format("%s %s:%s", message, type.getDescription(), value));
    }

}
