package com.fhs.platform.service.user.controller.response;

import com.fhs.platform.service.user.constants.gender.Gender;
import java.util.UUID;
import lombok.Data;

@Data
public class ContactResponseDto {

    private UUID id;
    private String name;
    private Gender gender;
    private String email;
    private String phoneNumber;
    private String organization;
    private String relationship;
    private AddressResponseDto address;

}
