package com.fhs.platform.service.user.controller.request.update;

import java.util.Set;
import lombok.Data;

@Data
public class PatientUpdateDto {

    private String phoneNumber;
    private String familyName;
    private String givenName;
    private String prefixName;
    private String suffixName;
    private String email;
    private String gender;
    private String birthDate;
    private String maritalStatus;
    private String managingOrganization;
    private Set<IdentifierUpdateDto> identifiers;
    private Set<AddressUpdateDto> addresses;
    private Set<ContactUpdateDto> contacts;

}
