package com.fhs.platform.service.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fhs.platform.common.entity.BaseEntity;
import com.fhs.platform.service.user.constants.gender.Gender;
import com.fhs.platform.service.user.constants.patientcontactrelationship.PatientContactRelationship;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = false)
@Data
@NoArgsConstructor
public class Contact extends BaseEntity {

    private String name;
    private Gender gender;
    private String email;
    private String phoneNumber;
    private String organization;
    private PatientContactRelationship relationship;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    @EqualsAndHashCode.Exclude
    private Address address;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    @JsonIgnore
    private Patient patient;

}
