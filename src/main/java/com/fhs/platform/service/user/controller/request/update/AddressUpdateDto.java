package com.fhs.platform.service.user.controller.request.update;

import lombok.Data;

@Data
public class AddressUpdateDto {

    private String use;
    private String type;
    private String text;
    private String line;
    private String city;
    private String district;
    private String state;
    private String postalCode;
    private String country;

}
