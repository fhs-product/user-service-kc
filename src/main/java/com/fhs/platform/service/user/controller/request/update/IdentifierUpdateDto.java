package com.fhs.platform.service.user.controller.request.update;

import lombok.Data;

@Data
public class IdentifierUpdateDto {

    private String identifierType;
    private String value;
    private String startValid;
    private String endValid;
    private String assigner;

}
