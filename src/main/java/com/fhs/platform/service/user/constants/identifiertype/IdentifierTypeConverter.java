package com.fhs.platform.service.user.constants.identifiertype;


import java.util.stream.Stream;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class IdentifierTypeConverter implements AttributeConverter<IdentifierType, String> {

    @Override
    public String convertToDatabaseColumn(IdentifierType identifierType) {
        if (identifierType == null) {
            return null;
        }
        return identifierType.getCode();
    }

    @Override
    public IdentifierType convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
        return Stream.of(IdentifierType.values()).filter(c -> c.getCode().equals(code)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

}
