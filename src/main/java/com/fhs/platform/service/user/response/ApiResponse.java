package com.fhs.platform.service.user.response;

import com.fhs.platform.service.user.constants.responsestatus.ResponseStatus;
import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class ApiResponse {

    private ResponseStatus status;
    private String message;

}
