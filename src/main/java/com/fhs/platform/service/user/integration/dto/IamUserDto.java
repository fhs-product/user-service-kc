package com.fhs.platform.service.user.integration.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IamUserDto {
    private UUID userId;
    private String username;
    private String password;
    private String authority;
    private String roleName;

}
