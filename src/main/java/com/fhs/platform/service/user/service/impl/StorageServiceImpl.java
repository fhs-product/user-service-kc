package com.fhs.platform.service.user.service.impl;

import com.fhs.platform.common.errors.exceptions.ResourceNotFoundException;
import com.fhs.platform.service.user.config.filestorage.StorageProperties;
import com.fhs.platform.service.user.exceptions.StorageException;
import com.fhs.platform.service.user.service.StorageService;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class StorageServiceImpl implements StorageService {

    private final Path rootLocation;

    @Autowired
    public StorageServiceImpl(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public Path store(MultipartFile file, UUID entityId) {
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file.");
            }
            Path folder = this.rootLocation.resolve(Paths.get(entityId.toString()));
            if (!Files.exists(folder)) {
                Files.createDirectories(folder);
            }
            Path destinationFile = folder.resolve(
                    Paths.get(Objects.requireNonNull(file.getOriginalFilename())))
                    .normalize().toAbsolutePath();
            if (!destinationFile.getParent().equals(folder.toAbsolutePath())) {
                throw new StorageException(
                        "Cannot store file outside current directory.");
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, destinationFile,
                        StandardCopyOption.REPLACE_EXISTING);
            }
            return destinationFile;
        } catch (IOException e) {
            throw new StorageException("Failed to store file.", e);
        }
    }

    @Override
    public Stream<Path> loadAll(UUID entityId) {
        Path folder = this.rootLocation.resolve(Paths.get(entityId.toString()));
        try {
            if(!Files.exists(folder)){
                Files.createDirectories(folder);
            }
            return Files.walk(folder, 1)
                    .filter(path -> !path.equals(folder))
                    .map(folder::relativize);
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }


    @Override
    public Resource loadAsResource(String filename, UUID entityId) {
        try {
            Path file = rootLocation.resolve(entityId.toString()).resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new ResourceNotFoundException(filename);
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void deleteAll(UUID entityId) {
        FileSystemUtils.deleteRecursively(rootLocation.resolve(entityId.toString()).toFile());
    }

}
