package com.fhs.platform.service.user.repository;

import com.fhs.platform.service.user.entity.Practitioner;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PractitionerRepository extends JpaRepository<Practitioner, UUID>, JpaSpecificationExecutor<Practitioner> {

    Collection<Practitioner> findByIsDeleted(boolean isDeleted);

    Optional<Practitioner> findByIdAndIsDeleted(UUID id, boolean isDeleted);

}
