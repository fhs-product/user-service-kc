package com.fhs.platform.service.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fhs.platform.common.entity.BaseEntity;
import com.fhs.platform.service.user.constants.gender.Gender;
import com.fhs.platform.service.user.constants.role.Role;
import com.fhs.platform.service.user.constants.specialty.Specialty;
import java.time.ZonedDateTime;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
public class Practitioner extends BaseEntity {
    private String avatarUrl;
    @NotNull
    private Gender gender;
    @NotNull
    private String familyName;
    @NotNull
    private String givenName;
    @NotNull
    private Boolean active = true;
    @NotNull
    private String prefixName;
    private String suffixName;
    @NotNull
    private ZonedDateTime birthDate;
    @NotNull
    private Role role = Role.Practitioner;
    @NotNull
    private Integer experience;
    @NotNull
    private String email;
    @JsonIgnore
    @ManyToMany(mappedBy = "practitioners", fetch = FetchType.LAZY)
    private Collection<Patient> patients;

    @OneToMany(mappedBy = "practitioner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<Qualification> qualifications;

    private Specialty specialty;

    @OneToMany(mappedBy = "practitioner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<Address> addresses;

    @OneToMany(mappedBy = "practitioner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<Identifier> identifiers;

}
