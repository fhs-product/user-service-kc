package com.fhs.platform.service.user.controller;

import com.fhs.platform.common.config.auth.BearerContextHolder;
import com.fhs.platform.common.dto.DeleteResponse;
import com.fhs.platform.common.dto.PagingResponseDTO;
import com.fhs.platform.service.user.constants.responsestatus.ResponseStatus;
import com.fhs.platform.service.user.controller.request.create.AddressCreateDto;
import com.fhs.platform.service.user.controller.request.create.IdentifierCreateDto;
import com.fhs.platform.service.user.controller.request.create.PractitionerCreateDto;
import com.fhs.platform.service.user.controller.request.create.QualificationCreateDto;
import com.fhs.platform.service.user.controller.request.update.AddressUpdateDto;
import com.fhs.platform.service.user.controller.request.update.IdentifierUpdateDto;
import com.fhs.platform.service.user.controller.request.update.PractitionerUpdateDto;
import com.fhs.platform.service.user.controller.request.update.QualificationUpdateDto;
import com.fhs.platform.service.user.controller.response.AddressResponseDto;
import com.fhs.platform.service.user.controller.response.IdentifierResponseDto;
import com.fhs.platform.service.user.controller.response.PractitionerResponseDto;
import com.fhs.platform.service.user.controller.response.QualificationResponseDto;
import com.fhs.platform.service.user.domain.PractitionerFilter;
import com.fhs.platform.service.user.exceptions.EntityAlreadyExists;
import com.fhs.platform.service.user.response.ApiResponse;
import com.fhs.platform.service.user.service.PractitionerService;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PractitionerController {

    private final PractitionerService practitionerService;


    @GetMapping(value = "/practitioners")
    public ResponseEntity<?> getPractitioners(@PageableDefault(sort = {"familyName"}, size = 10) Pageable pageable,
            PractitionerFilter filter) {
        if (BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("PATIENT") || BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("ADMIN")) {
            PagingResponseDTO<?> practitionerDtos = practitionerService.getPractitioners(pageable, filter);
            if (practitionerDtos.getData().isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(practitionerDtos, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }

    }


    @GetMapping(value = "/practitioners/{practitionerId}")

    public ResponseEntity<?> getPractitionerById(@PathVariable("practitionerId") UUID practitionerId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else if (!BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("patient")) {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        PractitionerResponseDto practitionerDto = practitionerService.getPractitionerById(practitionerId);
        return new ResponseEntity<>(practitionerDto, HttpStatus.OK);

    }

    @GetMapping(value = "/practitioners/{practitionerId}/addresses/{addressId}")
    public ResponseEntity<?> getPractitionerAddressById(@PathVariable("practitionerId") UUID practitionerId,
            @PathVariable("addressId") UUID addressId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {

                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        AddressResponseDto addressResponseDto = practitionerService.getPractitionerAddressById(practitionerId, addressId);
        return new ResponseEntity<>(addressResponseDto, HttpStatus.OK);

    }

    @GetMapping(value = "/practitioners/{practitionerId}/identifiers/{identifierId}")
    public ResponseEntity<?> getPractitionerIdentifierById(@PathVariable("practitionerId") UUID practitionerId,
            @PathVariable("identifierId") UUID identifierId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else if (!BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("patient")) {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        IdentifierResponseDto identifierResponseDto = practitionerService.getPractitionerIdentifierById(practitionerId, identifierId);
        return new ResponseEntity<>(identifierResponseDto, HttpStatus.OK);

    }

    @GetMapping(value = "/practitioners/{practitionerId}/qualifications/{qualificationId}")
    public ResponseEntity<?> getPractitionerQualificationById(@PathVariable("practitionerId") UUID practitionerId,
            @PathVariable("qualificationId") UUID qualificationId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        QualificationResponseDto createQualificationDto = practitionerService.getPractitionerQualificationById(practitionerId, qualificationId);
        return new ResponseEntity<>(createQualificationDto, HttpStatus.OK);

    }

    @GetMapping(value = "/practitioners/{practitionerId}/addresses")
    public ResponseEntity<?> getPractitionerAddresses(@PathVariable UUID practitionerId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        List<AddressResponseDto> createAddressDtos = practitionerService.getPractitionerAddress(practitionerId);
        if (createAddressDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(createAddressDtos, HttpStatus.OK);

    }

    @GetMapping(value = "/practitioners/{practitionerId}/identifiers")
    public ResponseEntity<?> getPractitionerIdentifiers(@PathVariable UUID practitionerId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        List<IdentifierResponseDto> identifiersDto = practitionerService.getPractitionerIdentifiers(practitionerId);
        if (identifiersDto.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(identifiersDto, HttpStatus.OK);


    }

    @GetMapping(value = "/practitioners/{practitionerId}/qualifications")
    public ResponseEntity<?> getPractitionerQualifications(@PathVariable UUID practitionerId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        List<QualificationResponseDto> createQualificationDtos = practitionerService.getPractitionerQualifications(practitionerId);
        if (createQualificationDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(createQualificationDtos, HttpStatus.OK);

    }


    @PostMapping(value = "/practitioners")
    public ResponseEntity<?> createPractitioner(@RequestBody @Valid PractitionerCreateDto practitionerCreateDto) {
        try {
            PractitionerResponseDto practitionerResponseDto = practitionerService.createPractitioner(practitionerCreateDto);
            return new ResponseEntity<>(practitionerResponseDto, HttpStatus.CREATED);

        } catch (EntityAlreadyExists e) {
            return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/practitioners/{practitionerId}/addresses")
    public ResponseEntity<?> createPractitionerAddress(@PathVariable UUID practitionerId, @RequestBody @Valid AddressCreateDto addressCreateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        AddressResponseDto addressResponseDto = practitionerService.createPractitionerAddress(practitionerId, addressCreateDto);
        return new ResponseEntity<>(addressResponseDto, HttpStatus.CREATED);

    }

    @PostMapping(value = "/practitioners/{practitionerId}/identifiers")
    public ResponseEntity<?> createPractitionerIdentifier(@PathVariable UUID practitionerId,
            @RequestBody @Valid IdentifierCreateDto identifierCreateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        try {
            IdentifierResponseDto identifierResponseDto = practitionerService.createPractitionerIdentifier(practitionerId, identifierCreateDto);
            return new ResponseEntity<>(identifierResponseDto, HttpStatus.CREATED);
        } catch (EntityAlreadyExists e) {
            return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/practitioners/{practitionerId}/qualifications")
    public ResponseEntity<?> createPractitionerQualification(@PathVariable UUID practitionerId,
            @RequestBody @Valid QualificationCreateDto qualificationCreateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        QualificationResponseDto qualificationResponseDto = practitionerService
                .createPractitionerQualification(practitionerId, qualificationCreateDto);
        return new ResponseEntity<>(qualificationResponseDto, HttpStatus.CREATED);

    }

    @PutMapping(value = "/practitioners/{practitionerId}")
    public ResponseEntity<?> updatePatient(@PathVariable("practitionerId") UUID practitionerId, @RequestBody
            PractitionerUpdateDto practitionerUpdateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        PractitionerResponseDto practitionerResponseDto = practitionerService.updatePractitionerById(practitionerId, practitionerUpdateDto);
        return new ResponseEntity<>(practitionerResponseDto, HttpStatus.OK);

    }

    @PutMapping(value = "/practitioners/{practitionerId}/addresses/{addressId}")
    public ResponseEntity<?> updatePractitionerAddressById(@PathVariable("practitionerId") UUID practitionerId,
            @PathVariable("addressId") UUID addressId,
            @RequestBody AddressUpdateDto addressUpdateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        AddressResponseDto addressResponseDto = practitionerService.updatePractitionerAddressById(practitionerId, addressId, addressUpdateDto);
        return new ResponseEntity<>(addressResponseDto, HttpStatus.OK);

    }


    @PutMapping(value = "/practitioners/{practitionerId}/identifiers/{identifierId}")
    public ResponseEntity<?> updatePractitionerIdentifierById(@PathVariable("practitionerId") UUID practitionerId,
            @PathVariable("identifierId") UUID identifierId,
            @RequestBody IdentifierUpdateDto identifierUpdateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        try {
            IdentifierResponseDto identifierResponseDto = practitionerService
                    .updatePractitionerIdentifierById(practitionerId, identifierId, identifierUpdateDto);
            return new ResponseEntity<>(identifierResponseDto, HttpStatus.OK);
        } catch (EntityAlreadyExists e) {
            return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/practitioners/{practitionerId}/qualifications/{qualificationId}")
    public ResponseEntity<?> updatePractitionerQualificationById(@PathVariable("practitionerId") UUID practitionerId,
            @PathVariable("qualificationId") UUID qualificationId,
            @RequestBody QualificationUpdateDto qualificationUpdateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        QualificationResponseDto qualificationResponseDto = practitionerService
                .updatePractitionerQualificationById(practitionerId, qualificationId, qualificationUpdateDto);
        return new ResponseEntity<>(qualificationResponseDto, HttpStatus.OK);

    }

    @DeleteMapping(value = "/practitioners/{practitionerId}/addresses/{addressId}")
    public ResponseEntity<?> deletePractitionerAddressById(@PathVariable("practitionerId") UUID practitionerId,
            @PathVariable("addressId") UUID addressId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        DeleteResponse addressResponseDto = practitionerService.deletePractitionerAddressById(practitionerId, addressId);
        return new ResponseEntity<>(addressResponseDto, HttpStatus.OK);

    }

    @DeleteMapping(value = "/practitioners/{practitionerId}/identifiers/{identifierId}")
    public ResponseEntity<?> deletePractitionerIdentifierById(@PathVariable("patientId") UUID practitionerId,
            @PathVariable("identifierId") UUID identifierId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        DeleteResponse identifierResponseDto = practitionerService.deletePractitionerIdentifierById(practitionerId, identifierId);
        return new ResponseEntity<>(identifierResponseDto, HttpStatus.OK);

    }

    @DeleteMapping(value = "/practitioners/{practitionerId}/qualifications/{qualificationId}")
    public ResponseEntity<?> deletePractitionerQualificationById(@PathVariable("practitionerId") UUID practitionerId,
            @PathVariable("qualificationId") UUID qualificationId) {

        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }

        DeleteResponse qualificationResponseDto = practitionerService
                .deletePractitionerQualificationById(practitionerId, qualificationId);
        return new ResponseEntity<>(qualificationResponseDto, HttpStatus.OK);

    }

    @DeleteMapping(value = "/practitioners/{practitionerId}")
    public ResponseEntity<?> deletePractitionerById(@PathVariable("practitionerId") UUID practitionerId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("practitioner")) {
                practitionerId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        DeleteResponse practitionerDto = practitionerService.deletePractitionerById(practitionerId);
        return new ResponseEntity<>(practitionerDto, HttpStatus.OK);

    }

}
