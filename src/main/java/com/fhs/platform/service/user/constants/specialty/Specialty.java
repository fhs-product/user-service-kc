package com.fhs.platform.service.user.constants.specialty;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Specialty {
    AdultMentalIllness("408467006", "Adult mental illness"),
    Cardiology("394579002", "Cardiology");
    private final String code;
    private final String description;

}
