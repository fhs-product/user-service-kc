package com.fhs.platform.service.user.util.mapper;

import com.fhs.platform.service.user.controller.request.create.IdentifierCreateDto;
import com.fhs.platform.service.user.controller.request.update.IdentifierUpdateDto;
import com.fhs.platform.service.user.controller.response.IdentifierResponseDto;
import com.fhs.platform.service.user.entity.Identifier;
import org.mapstruct.BeanMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = {
}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface IdentifierMapper {

    @Mapping(target = "patient", ignore = true)
    Identifier identifierDtoToIdentifier(IdentifierCreateDto identifierCreateDto);
    @Mapping(target = "startValid", dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX")
    @Mapping(target = "endValid", dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX")
    IdentifierResponseDto identifierToIdentifierResponseDto(Identifier identifier);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateIdentifierFromDto(IdentifierUpdateDto identifierUpdateDto, @MappingTarget Identifier identifier);

}
