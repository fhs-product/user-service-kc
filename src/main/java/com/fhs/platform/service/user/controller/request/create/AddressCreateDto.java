package com.fhs.platform.service.user.controller.request.create;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class AddressCreateDto {

    @NotNull
    private String use;
    @NotNull
    private String type;
    @NotNull
    private String text;
    @NotNull
    private String line;
    @NotNull
    private String city;
    @NotNull
    private String district;
    private String state;
    @NotNull
    private String postalCode;
    @NotNull
    private String country;

}
