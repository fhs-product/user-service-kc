package com.fhs.platform.service.user.constants.addresstype;


import java.util.stream.Stream;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Converter(autoApply = true)
public class AddressTypeConverter implements AttributeConverter<AddressType, String> {

    @Override
    public String convertToDatabaseColumn(AddressType addressType) {
        if (addressType == null) {
            return null;
        }
        return addressType.getCode();
    }

    @Override
    public AddressType convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
        return Stream.of(AddressType.values()).filter(c -> c.getCode().equals(code)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

}
