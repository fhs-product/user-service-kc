package com.fhs.platform.service.user.util.search;

import com.fhs.platform.service.user.constants.gender.Gender;
import com.fhs.platform.service.user.domain.PractitionerFilter;
import com.fhs.platform.service.user.entity.Practitioner;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

@RequiredArgsConstructor
public class PractitionerSpecification implements Specification<Practitioner> {

    private final PractitionerFilter filter;

    @Override
    public Predicate toPredicate(Root<Practitioner> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(root.get("isDeleted"), false));
        if (StringUtils.isNotBlank(filter.getGender())) {
            predicates.add(criteriaBuilder.equal(root.get("gender"), Gender.valueOf(filter.getGender())));
        }
        if (StringUtils.isNotBlank(filter.getName())) {
            System.out.println(filter.getName());
            Expression<String> familyNameConcatGivenName = criteriaBuilder.concat(root.get("familyName"), " ");
            familyNameConcatGivenName = criteriaBuilder.concat(familyNameConcatGivenName, root.get("givenName"));
            Expression<String> givenNameConcatFamilyName = criteriaBuilder.concat(root.<String>get("givenName"), " ");
            givenNameConcatFamilyName = criteriaBuilder.concat(givenNameConcatFamilyName, root.get("familyName"));
            Predicate whereClause = criteriaBuilder.or(criteriaBuilder.like(

                    criteriaBuilder.lower(familyNameConcatGivenName), "%" + filter.getName().toLowerCase() + "%"), criteriaBuilder.like(

                    criteriaBuilder.lower(givenNameConcatFamilyName), "%" + filter.getName().toLowerCase() + "%"));
            predicates.add(whereClause);
        }

        if (StringUtils.isNotBlank(filter.getStartBirthDate())) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("birthDate"), filter.getStartBirthDate()));
        }
        if (StringUtils.isNotBlank(filter.getEndBirthDate())) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("birthDate"), filter.getEndBirthDate()));
        }
        if (StringUtils.isNotBlank(filter.getId())) {
            predicates.add(criteriaBuilder.equal(root.get("id"), UUID.fromString(filter.getId())));
        }
        if (StringUtils.isNotBlank(filter.getStartBirthDate())) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("experience"), Integer.parseInt(filter.getMinimumExperience())));
        }
        if (StringUtils.isNotBlank(filter.getEndBirthDate())) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("experience"), Integer.parseInt(filter.getMaximumExperience())));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

}
