package com.fhs.platform.service.user.controller.response;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fhs.platform.common.config.jackson.ZonedDateTimeDeserializer;
import com.fhs.platform.common.config.jackson.ZonedDateTimeSerializer;
import com.fhs.platform.common.constants.DateTimeFormatConstants;
import lombok.Data;

@Data

public class PractitionerResponseDto {

    private UUID id;
    private String avatarUrl;
    private String familyName;
    private String givenName;
    private String prefixName;
    private String suffixName;
    private String gender;
    @JsonDeserialize(using = ZonedDateTimeDeserializer.class)
    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormatConstants.ZONE_DATE_TIME_FORMAT)
    private ZonedDateTime  birthDate;
    private String email;
    private String experience;
    private Set<QualificationResponseDto> qualifications;
    private String specialty;
    private Set<AddressResponseDto> addresses;
    private Set<IdentifierResponseDto> identifiers;

}
