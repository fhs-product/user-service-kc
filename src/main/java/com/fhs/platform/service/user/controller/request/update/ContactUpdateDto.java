package com.fhs.platform.service.user.controller.request.update;

import com.fhs.platform.service.user.constants.gender.Gender;
import lombok.Data;

@Data
public class ContactUpdateDto {

    private String name;
    private Gender gender;
    private String email;
    private String phoneNumber;
    private String organization;
    private String relationship;
    private AddressUpdateDto address;

}
