package com.fhs.platform.service.user.repository;

import com.fhs.platform.service.user.entity.Qualification;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QualificationRepository extends JpaRepository<Qualification, UUID> {

}
