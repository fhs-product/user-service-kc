package com.fhs.platform.service.user.constants.patientcontactrelationship;

import java.util.stream.Stream;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)

public class PatientContactRelationshipConverter implements AttributeConverter<PatientContactRelationship, Character> {

    @Override
    public Character convertToDatabaseColumn(PatientContactRelationship attribute) {
        if (attribute == null) {
            return 'U';
        }
        return attribute.getCode();
    }

    @Override
    public PatientContactRelationship convertToEntityAttribute(Character dbData) {
        if (dbData == null) {
            return null;
        }
        return Stream.of(PatientContactRelationship.values()).filter(c -> c.getCode().equals(dbData)).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
