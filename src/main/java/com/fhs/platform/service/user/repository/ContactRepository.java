package com.fhs.platform.service.user.repository;

import com.fhs.platform.service.user.entity.Contact;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository extends JpaRepository<Contact, UUID> {

}
