
package com.fhs.platform.common.logging.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fhs.platform.common.constants.CommonMessages;
import com.fhs.platform.common.errors.exceptions.BusinessException;
import com.fhs.platform.common.errors.exceptions.ErrorDetail;
import com.fhs.platform.common.errors.exceptions.ErrorMessage;
import com.fhs.platform.common.errors.exceptions.ResourceNotFoundException;
import com.fhs.platform.common.logging.dto.LogControllerRequest;
import com.fhs.platform.common.logging.dto.LogControllerResponse;
import com.fhs.platform.common.logging.util.JSONUtil;
import com.fhs.platform.common.utils.MessageUtils;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Created by vietnguyen on 9/15/20
 */

@Aspect
public class LoggingAspect {

    private static final String ENTER_METHOD_LOG_MSG = "Enter: {}() with argument[s] = {}";
    private static final String EXIT_METHOD_LOG_MSG = "Exit: {}() with result = {}";
    private static final String EXCEPTION_WHEN_LOGGING_MSG = "Exception when logging {}() with cause = {}";
    private Set<String> maskableKeys = new HashSet<>();

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MessageUtils messageUtils;

    @Value("${microservice.error.prefix}")
    private String prefix;

    @Value("${maskableKeys:#{null}}")
    private void setMaskableKeys(String[] maskableKeys) {
        if (maskableKeys != null && maskableKeys.length > 0) {
            this.maskableKeys = Arrays.stream(maskableKeys).map(String::toLowerCase).collect(Collectors.toSet());
        }
    }

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void applicationControllerPointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    @Pointcut("@annotation(com.fhs.platform.common.logging.config.LoggingAnnotation.NoLogging)")
    public void noLoggingPointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    @Pointcut("within(@org.springframework.stereotype.Service *)")
    public void applicationServicePointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    @Pointcut("within(@com.fhs.platform.common.logging.config.LoggingAnnotation.ControllerLogging *)")
    public void baseControllerPointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    @Pointcut("within(@com.fhs.platform.common.logging.config.LoggingAnnotation.ServiceLogging *)")
    public void baseServicePointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Advice that logs when a method is entered and exited.
     *
     * @param joinPoint join point for advice.
     * @return result.
     * @throws Exception throws {@link IllegalArgumentException}.
     */
    @Around("(applicationControllerPointcut() || baseControllerPointcut() ) " + "&& !noLoggingPointcut()")
    public Object logControllerInfo(ProceedingJoinPoint joinPoint) throws Throwable {
        Logger log = logger(joinPoint);
        LogControllerRequest logRequest = null;
        try {
            logRequest = buildLogControllerRequest(joinPoint);
            ObjectNode request = objectMapper.createObjectNode();
            request.putPOJO("request", logRequest);
            String requestLogStr = JSONUtil.parseObjectToString(request, maskableKeys);
            log.info(requestLogStr);
        } catch (Exception exception) {
            log.error(EXCEPTION_WHEN_LOGGING_MSG, joinPoint.getSignature().getName(), exception.getCause() != null ? exception.getCause() : "NULL");
        }
        long startTime = System.currentTimeMillis();
        Object response = joinPoint.proceed();
        try {
            long endTime = System.currentTimeMillis();
            LogControllerResponse logResponse = buildLogControllerResponse(response);
            logResponse.setProcessingTime(endTime - startTime + " ms");
            logResponse.setRequest(logRequest);
            String responseLogStr = JSONUtil.parseObjectToString(logResponse, maskableKeys);
            log.info(responseLogStr);
        } catch (Exception exception) {
            log.error(EXCEPTION_WHEN_LOGGING_MSG, joinPoint.getSignature().getName(), exception.getCause() != null ? exception.getCause() : "NULL");
        }
        return response;
    }

    /**
     * Advice that logs when a method is entered and exited.
     *
     * @param joinPoint join point for advice.
     * @return result.
     * @throws Exception throws {@link IllegalArgumentException}.
     */
    @Around("(applicationServicePointcut() || baseServicePointcut() ) " + "&& " +
            "!noLoggingPointcut()")
    public Object logServiceInfo(ProceedingJoinPoint joinPoint) throws Throwable {
        Logger log = logger(joinPoint);
        String arguments = Arrays.toString(joinPoint.getArgs());
        log.debug(ENTER_METHOD_LOG_MSG, joinPoint.getSignature().getName(), arguments);
        Object result = joinPoint.proceed();
        try {
            String methodResultStr = JSONUtil.parseObjectToString(result, maskableKeys);
            log.debug(EXIT_METHOD_LOG_MSG,
                    joinPoint.getSignature().getName(), methodResultStr);
        } catch (Exception exception) {
            log.error(EXCEPTION_WHEN_LOGGING_MSG, joinPoint.getSignature().getName(), exception.getCause() != null ? exception.getCause() : "NULL");
        }
        return result;
    }


    /**
     * Advice that logs methods in controller throwing exceptions.
     *
     * @param joinPoint join point for advice
     * @param e         exception
     */
    @AfterThrowing(pointcut =
            "( applicationControllerPointcut() || baseControllerPointcut() )" + "&& " + "!noLoggingPointcut()", throwing = "e")
    public void logAfterThrowingController(JoinPoint joinPoint, Exception e) {
        Logger log = logger(joinPoint);
        try {
            LogControllerRequest logRequest = buildLogControllerRequest(joinPoint);
            ObjectNode request = objectMapper.createObjectNode();
            request.putPOJO("request", logRequest);
            String requestLogStr = JSONUtil.parseObjectToString(request, maskableKeys);
            logExceptionHandling(joinPoint, e, requestLogStr);
        } catch (Exception exception) {
            log.error(EXCEPTION_WHEN_LOGGING_MSG, joinPoint.getSignature().getName(), exception.getCause() != null ? exception.getCause() : "NULL");
        }
    }

    /**
     * Advice that logs methods in service throwing exceptions.
     *
     * @param joinPoint join point for advice
     * @param e         exception
     */
    @AfterThrowing(pointcut = " ( applicationServicePointcut() || baseServicePointcut() )  "
            + "&& " + "!noLoggingPointcut()", throwing = "e")
    public void logAfterThrowingService(JoinPoint joinPoint, Exception e) {
        Logger log = logger(joinPoint);
        try {
            String requestLogStr = String.format("Method: %s() - argument[s] : %s ", joinPoint.getSignature().getName(),
                    Arrays.toString(joinPoint.getArgs()));
            logExceptionHandling(joinPoint, e, requestLogStr);
        } catch (Exception exception) {
            log.error(EXCEPTION_WHEN_LOGGING_MSG, joinPoint.getSignature().getName(), exception.getCause() != null ? exception.getCause() : "NULL");
        }
    }

    private void logExceptionHandling(JoinPoint joinPoint, Exception e, String requestLogStr) {
        Logger logger = logger(joinPoint);
        String statusCode = HttpStatus.BAD_REQUEST.toString();
        List<ErrorDetail> errors = new ArrayList<>();
        // Building status & error messages
        if (e instanceof BusinessException) {
            BusinessException ex = (BusinessException) e;
            String errCode = CommonMessages.HTTP_BAD_REQUEST;
            ErrorMessage errorMessage = ex.getErrorMessage(prefix, messageUtils);
            if (errorMessage != null) {
                if (HttpStatus.FORBIDDEN == errorMessage.getStatus()) {
                    statusCode = HttpStatus.FORBIDDEN.toString();
                    errCode = CommonMessages.HTTP_ACCESS_DENIED;
                }
                if (errorMessage.getIssues() != null && errorMessage.getIssues().size() > 0) {
                    errors.addAll(errorMessage.getIssues());
                    statusCode = errorMessage.getStatus().toString();
                } else {
                    errors.add(new ErrorDetail(errCode, errorMessage.getMessage()));
                }
            }
        } else if (e instanceof ResourceNotFoundException) {
            statusCode = HttpStatus.NOT_FOUND.toString();
            errors.add(new ErrorDetail(CommonMessages.HTTP_PAGE_NOT_FOUND, ""));
        } else {
            statusCode = HttpStatus.INTERNAL_SERVER_ERROR.toString();
            errors.add(new ErrorDetail(CommonMessages.HTTP_INTERNAL_SERVER_ERROR, e.getMessage()));
        }
        if (!errors.isEmpty()) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.setIssues(errors);
            String response = showResponse(errorMessage, statusCode, requestLogStr);
            logger.error(response);
        }
    }

    private LogControllerRequest buildLogControllerRequest(JoinPoint joinPoint) {
        LogControllerRequest logRequest = new LogControllerRequest();
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        logRequest.setTimestamp(LocalDateTime.now().toString());
        logRequest.setRequestParameters(getParameter(method, joinPoint.getArgs()));
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        ((MethodSignature) joinPoint.getSignature()).getMethod().getParameterAnnotations();
        if (attributes != null) {
            HttpServletRequest request = attributes.getRequest();
            logRequest.setRequestMethod(request.getMethod());
            logRequest.setRequestUrl(request.getRequestURL().toString());
        }
        return logRequest;
    }

    private LogControllerResponse buildLogControllerResponse(Object response) {
        Object result = response;
        LogControllerResponse logResponse = new LogControllerResponse();
        logResponse.setStatus(String.valueOf(HttpStatus.OK.value()));
        if (result instanceof ResponseEntity) {
            ResponseEntity<?> responseEntity = (ResponseEntity<?>) result;
            response = responseEntity.getBody();
            logResponse.setStatus(String.valueOf(responseEntity.getStatusCode().value()));
        }
        logResponse.setResponse(response);
        return logResponse;
    }

    private List<Object> getParameter(Method method, Object[] args) {
        List<Object> argList = new ArrayList<>();
        Parameter[] parameters = method.getParameters();
        for (int i = 0; i < parameters.length; i++) {
            RequestBody requestBody = parameters[i].getAnnotation(RequestBody.class);
            if (requestBody != null) {
                argList.add(args[i]);
                continue;
            }
            getOtherParameters(args, argList, parameters, i);
        }
        return argList;

    }

    private void getOtherParameters(Object[] args, List<Object> argList, Parameter[] parameters, int index) {
        RequestParam requestParam = parameters[index].getAnnotation(RequestParam.class);
        if (requestParam != null) {
            Map<String, Object> map = new HashMap<>();
            String key = parameters[index].getName();
            if (!StringUtils.isEmpty(requestParam.value())) {
                key = requestParam.value();
            }
            map.put(key, args[index]);
            argList.add(map);
            return;
        }
        PathVariable pathVariable = parameters[index].getAnnotation(PathVariable.class);
        if (pathVariable != null) {
            Map<String, Object> map = new HashMap<>();
            String key = parameters[index].getName();
            if (!StringUtils.isEmpty(pathVariable.value())) {
                key = pathVariable.value();
            }
            map.put(key, args[index]);
            argList.add(map);
            return;
        }
        // escape the argument of Swagger API
        if (args[index] instanceof HttpServletRequest) {
            return;
        }
        //if the parameter is NOT annotated with RequestBody, PathVariable or RequestParam
        argList.add(args[index]);
    }

    /**
     * Retrieves the {@link Logger} associated to the given {@link JoinPoint}.
     *
     * @param joinPoint join point we want the logger for.
     * @return {@link Logger} associated to the given {@link JoinPoint}.
     */
    private Logger logger(JoinPoint joinPoint) {
        return LoggerFactory.getLogger(joinPoint.getSignature().getDeclaringTypeName());
    }

    /**
     * Building response informations
     *
     * @param response
     * @param statusCode
     * @return String
     */
    private String showResponse(Object response, String statusCode, String request) {
        String body = "";
        if (response != null) {
            if (response instanceof ByteArrayResource) {
                body = "{}";
            } else {
                body = JSONUtil.parseObjectToString(response, maskableKeys);
            }
        } else {
            body = "{}";
        }

        // Building response
        StringBuilder template = new StringBuilder();
        template.append(String.format("request: %s - ", request));
        template.append(String.format("response: %s - ", body));
        template.append(String.format("status: %s}", statusCode));

        return template.toString();
    }


}
