/*
 * BrightInsight CONFIDENTIAL
 *
 * Copyright (c) 2019-2020 BrightInsight, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of BrightInsight. The intellectual and
 * technical concepts contained herein are proprietary to BrightInsight and may be covered by U.S. and
 * Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from BrightInsight. Access to the source code contained herein is hereby forbidden
 * to anyone except current BrightInsight employees, managers or contractors who have executed Confidentiality and
 * Non-disclosure agreements explicitly covering such access.
 * The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
 * which includes information that is confidential and/or proprietary, and is a trade secret, of BrightInsight Incorporated.
 *
 * ANY REPRODUCTION, MODIFICATION, DISTRIBUTION PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF
 * APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED
 * INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */

package com.fhs.platform.common.logging.dto;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by vietnguyen on 10/1/20
 */

@Getter
@Setter
public class LogControllerRequest {
    private String timestamp;
    private String requestMethod;
    private String requestUrl;
    private List<Object> requestParameters;

}
