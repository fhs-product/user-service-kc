package com.fhs.platform.common.config.token;

import io.jsonwebtoken.Claims;

public interface TokenInterpreter {

    Claims getClaimsFromToken(String jwtToken);

}
