package com.fhs.platform.common.config.token;

import com.fhs.platform.common.config.auth.BearerContext;
import com.fhs.platform.common.config.auth.BearerContextHolder;
import com.fhs.platform.common.constants.CommonMessages;
import com.fhs.platform.common.constants.Constants;
import com.fhs.platform.common.utils.TokenValidationUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@Order(1)

public class JWTAuthorizationTokenFilter extends OncePerRequestFilter {

    public static final String INVALID_TOKEN_MESSAGE = "Invalid token";
    public static final String INVALID_TOKEN_PARAM = "Invalid %s";
    public static final String USER_NAME_CLAIM_NAME = "user_name";
    public static final String USER_ID_CLAIM_NAME = "user_id";
    public static final String ROLE_CLAIM_NAME = "role";
    public static final String CLIENT_ID_CLAIM_NAME = "client_id";

    private final TokenInterpreter tokenProvider;

    public JWTAuthorizationTokenFilter(TokenInterpreter tokenProvider, TokenValidationUtil tokenValidationUtil) {
        this.tokenProvider = tokenProvider;
    }

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {
        try {
            if (containsToken(request)) {
                Claims claims = validateToken(request);
                Optional<String> userId = Optional.ofNullable((String) claims.get(USER_ID_CLAIM_NAME));
                Optional<String> roleName = Optional.ofNullable((String) claims.get(ROLE_CLAIM_NAME));

                BearerContext context = BearerContextHolder.getContext();
                context.setUserId(userId.orElseThrow(() ->
                        new MalformedJwtException(String.format(INVALID_TOKEN_PARAM, USER_ID_CLAIM_NAME))));
                context.setRoleName(roleName.orElseThrow(() ->
                        new MalformedJwtException(String.format(INVALID_TOKEN_PARAM, ROLE_CLAIM_NAME))));
                filterChain.doFilter(request, response);
            } else {
                createUnauthorizedResult(response, CommonMessages.HTTP_ACCESS_DENIED);
            }
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException e) {
            createUnauthorizedResult(response, e.getMessage());
        } finally {
            BearerContextHolder.clearContext();
        }
    }


    private boolean containsToken(HttpServletRequest request) {
        String authenticationHeader = request.getHeader(Constants.AUTHORIZATION_HEADER);
        return authenticationHeader != null && authenticationHeader.startsWith(Constants.TOKEN_PREFIX);
    }

    private Claims validateToken(HttpServletRequest request) {
        String jwtToken = request.getHeader(Constants.AUTHORIZATION_HEADER).replace(Constants.TOKEN_PREFIX,
                StringUtils.EMPTY);
        BearerContextHolder.getContext().setBearerToken(jwtToken);
        return tokenProvider.getClaimsFromToken(jwtToken);
    }

    private void createUnauthorizedResult(HttpServletResponse response, String message) throws IOException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, message);
    }

    @Override
    protected boolean shouldNotFilter(@NonNull HttpServletRequest request) {
        return skipUrls().stream()
                .anyMatch(p -> {
                    if (p.getLeft() != null) {
                        return p.getLeft().toString().equals(request.getMethod()) && new AntPathMatcher()
                                .match(p.getValue(), request.getServletPath());
                    }
                    return new AntPathMatcher().match(p.getValue(), request.getServletPath());
                });
    }

    private List<Pair<Object, String>> skipUrls() {
        return Arrays.asList(
                Pair.of(HttpMethod.OPTIONS, "/**"),
                Pair.of(HttpMethod.GET, "/actuator/health"),
                Pair.of(HttpMethod.GET, "/swagger-resources/**"),
                Pair.of(HttpMethod.GET, "/swagger-ui/**"),
                Pair.of(HttpMethod.POST, "/swagger-resources/**"),
                Pair.of(HttpMethod.POST, "/swagger-ui/**"),
                Pair.of(HttpMethod.GET, "/v2/api-docs"),
                Pair.of(HttpMethod.POST, "/v2/api-docs"),
                Pair.of(HttpMethod.POST, "/patients"),
                Pair.of(HttpMethod.POST, "/practitioners")

        );
    }

}

