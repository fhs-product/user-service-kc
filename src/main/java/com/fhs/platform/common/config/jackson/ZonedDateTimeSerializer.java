package com.fhs.platform.common.config.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ZonedDateTimeSerializer
        extends StdSerializer<ZonedDateTime> {

    private static DateTimeFormatter formatter =
            DateTimeFormatter.ISO_OFFSET_DATE_TIME;

    public ZonedDateTimeSerializer() {
        this(null);
    }
    public ZonedDateTimeSerializer(Class<ZonedDateTime> t) {
        super(t);
    }

    @Override
    public void serialize(
            ZonedDateTime value,
            JsonGenerator gen,
            SerializerProvider arg2)
            throws IOException, JsonProcessingException {
        gen.writeString(formatter.format(value.withZoneSameLocal(ZoneId.systemDefault())));
    }

}