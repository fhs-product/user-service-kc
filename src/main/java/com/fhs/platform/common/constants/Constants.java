
package com.fhs.platform.common.constants;

public class Constants {
    public static final String AUTHORIZATION_HEADER = "Authorization";

    public static final String TOKEN_PREFIX = "Bearer ";

    private Constants() {
        // Private constructor
    }

}
