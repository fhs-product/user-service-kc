package com.fhs.platform.common.dto;

import lombok.Data;

@Data
public class DeleteResponse {
    private final boolean success;
}
