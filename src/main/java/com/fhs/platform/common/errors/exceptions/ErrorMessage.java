
package com.fhs.platform.common.errors.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class ErrorMessage implements Serializable {

    private static final long serialVersionUID = 2486179963503999477L;

    private static final String EMPTY_MESSAGE = "Empty message";

    @JsonIgnore
    private String message;

    @JsonIgnore
    private HttpStatus status = HttpStatus.BAD_REQUEST;

    private List<ErrorDetail> issues = new ArrayList<>();

    public ErrorMessage() {
        super();
    }

    public ErrorMessage(String message) {
        super();
        this.message = message;
    }

    public ErrorMessage(String message, List<ErrorDetail> issues) {
        super();
        this.message = message;
        this.issues = issues;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        if (StringUtils.isBlank(message)) {
            message = EMPTY_MESSAGE;
        }
        return message;
    }

    public void addErrorDetail(ErrorDetail errorDetail) {
        issues.add(errorDetail);
    }

}
