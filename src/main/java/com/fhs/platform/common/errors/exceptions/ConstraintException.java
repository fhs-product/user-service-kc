
package com.fhs.platform.common.errors.exceptions;

import org.springframework.core.MethodParameter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

public class ConstraintException extends RuntimeException {

    private static final long serialVersionUID = -8323166939057094038L;

    private transient final MethodParameter parameter;
    private transient final BindingResult bindingResult;

    public ConstraintException(MethodParameter parameter, BindingResult bindingResult) {
        this.parameter = parameter;
        this.bindingResult = bindingResult;
    }

    public MethodParameter getParameter() {
        return this.parameter;
    }

    public BindingResult getBindingResult() {
        return this.bindingResult;
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder("Validation failed for argument [")
                .append(this.parameter.getParameterIndex())
                .append("] in ")
                .append(this.parameter.getExecutable().toGenericString());
        if (this.bindingResult.getErrorCount() > 1) {
            sb.append(" with ").append(this.bindingResult.getErrorCount()).append(" errors");
        }

        sb.append(": ");

        for (ObjectError error : this.bindingResult.getAllErrors()) {
            sb.append("[").append(error).append("] ");
        }

        return sb.toString();
    }

}
