ALTER TABLE user_service_schema.patient
ADD COLUMN if not exists avatar_url varchar(255);
ALTER TABLE user_service_schema.practitioner
ADD COLUMN if not exists avatar_url varchar(255);